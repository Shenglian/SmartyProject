<?php

ini_set('display_errors', 1);

class db {
	var $db = null;

	function __construct($host = "localhost", $dbname = "news", $user = "root", $pw = "root"){
		$dsn = "mysql:host=$host;dbname=$dbname";
		$this->db = new PDO($dsn, $user, $pw);
	}

	public function insert($query = ''){
		$this->db->exec($query);	
	}

	public function select($query = ''){
		
		$array = $this->db->query($query);

		$array->setFetchMode(PDO::FETCH_ASSOC);

		$array_fetchAll = $array->fetchAll();
		$array_column = array_column($array_fetchAll, 'name');

		foreach($array_column as $value){
		    echo $value . "<br>";
		}
	}

	public function close(){
		$this->db = null;
	}
}

?>