<?php

	class createTemplate {

		var $path = null;
		var $modelName = null;
		var $option = null;

		function __construct($modelName = '', $option = '', $path){
			$this->path = $path;
			$this->modelName = $modelName;
			$this->option = $option;
		}

		public function create(){
			if (file_exists($this->modelName)) {

				$fp = fopen($this->modelName, "r"); //只读打开模板
				$str = fread($fp, filesize($this->modelName)); //读取模板中内容

				foreach ($this->option as $key => $value) {
					$str = str_replace("$key", $value, $str);
					echo $str;
				}
			
				fclose($fp);

				$handle = fopen($this->path, "w"); //写入方式打开新闻路径

				fwrite($handle, $str); //把刚才替换的内容写进生成的HTML文件
				fclose($handle);

			} else {
				echo $this->modelName . ' 不存在！';
			}
		}

		public function result(){
			echo "<a href=$this->path target=_blank>查看刚才添加的新闻</a>"; 
		}

	}

?>